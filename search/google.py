# Модуль поиска в Google

import requests
import json
from search.search_item import SearchItem

# подключаем CSE - Гугл Кастом Серч Энджин
# get the API KEY here: https://developers.google.com/custom-search/v1/overview
API_KEY = "AIzaSyCBZYSEILsEnhl4LvwTIa9DazYRaILE7KY"
# get your Search Engine ID on your CSE control panel
SEARCH_ENGINE_ID = "9693f115facf460da"


def search(request='linux ubuntu', amount_results=11):
    """
    Поиск в Google по заданным параметрам.
    :param request: запрос пользователя
    :param amount_results: требуемое количество результатов поиска
    :return: список найденных объектов типа SearchItem
    """
    result = []

    words = request.split()  # разбиваем слова ппробелам
    url_request = '%20'.join(words)  # заменяем пробел на спецсимвол

    pages = amount_results // 10  # количество страниц
    if amount_results % 10 > 0:
        pages += 1

    # CSE возвращает только первые 10 результатов,
    # поэтому включаем итерацию
    for page in range(1, pages + 1):
        # calculating start, (page=2) => (start=11), (page=3) => (start=21)
        start = (page - 1) * 10 + 1

        url = f"https://www.googleapis.com/customsearch/v1?" \
              f"key={API_KEY}" \
              f"&cx={SEARCH_ENGINE_ID}" \
              f"&q={url_request}" \
              f"&start={start}"

        response = requests.get(url)
        obj = json.loads(response.text)  # json в объект

        for i, item in enumerate(obj["items"]):
            # если набрали нужное количество результатов поиска,
            # выходим из цикла
            if len(result) >= amount_results:
                break
            search_item = SearchItem(item["title"], item["link"])
            # добавляем в список найденных элементов
            result.append(search_item)
    return result
