# Модуль поиска в Yandex

from bs4 import BeautifulSoup
from search.search_item import SearchItem


def search(request='linux ubuntu', amount_results=11):
    """
    Поиск в Yandex по заданным параметрам.
    :param request: запрос пользователя
    :param amount_results: требуемое количество результатов поиска
    :return: список найденных объектов типа SearchItem
    """
    result = []

    # открыть файл-заглушку
    with open("yandex.html", encoding='utf-8') as fp:
        soup = BeautifulSoup(fp, "html.parser")
    # найти ссылки
    for link in soup.find_all("a", {"class": "OrganicTitle-Link"}):
        # если набрали нужное количество результатов поиска,
        # выходим из цикла
        if len(result) >= amount_results:
            break
        search_item = SearchItem(link.text, link.get("href"))
        # добавляем в список найденных элементов
        result.append(search_item)
    return result
