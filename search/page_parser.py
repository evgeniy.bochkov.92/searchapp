# Модуль универсального парсера страницы

import requests
from bs4 import BeautifulSoup

from search.search_item import SearchItem
from urllib.parse import urlparse


def search_recursively(url="https://aimp.ru", amount_results=1):
    """
    Рекурсивный поиск ссылок на страницах
    :param amount_results: требуемое количество результатов поиска
    :param url: адрес начальной страницы (получаем из поисковика)
    :return: список найденных объектов типа SearchItem
    """
    search_result = []
    while True:
        # если набрали нужное количество результатов поиска,
        # выходим из цикла
        if len(search_result) >= amount_results:
            break
        # находим первую ссылку на странице
        title, href = find_link(url)
        if href == "":
            break  # уходим, дальше искать нет смысла
        # проверить является ли ссылка относительной
        if is_relative(href):
            href = get_absolute(url, href)
        search_item = SearchItem(title, href)
        # добавляем в список найденных элементов
        search_result.append(search_item)
        url = href  # начальной становится новая страница
    return search_result


def is_relative(href="?do=awards"):
    if href.startswith("/") or href.startswith("?"):
        return True
    else:
        return False


def get_absolute(url="https://aimp.ru/", href="?do=awards"):
    """
    Попытка воссоздать полную ссылку
    :param url: полный адрес текущей страницы
    :param href: найденная ссылка (может содержать домен)
    :return: полный адрес найденной ссылки
    """
    domain = urlparse(url).netloc  # --> www.example.test
    scheme = urlparse(url).scheme  # --> https
    if href.startswith("/"):
        return f"{scheme}://{domain}{href}"
    return f"{scheme}://{domain}/{href}"


def find_link(url="https://aimp.ru"):
    """
    Поиск первой ссылки на странице
    :param url: адрес страницы, на которой
    требуется выполнить поиск ссылки
    :return: ссылка
    """
    title = href = ""
    try:
        response = requests.get(url)
    except:
        # иногда попадаются такие ссылки:
        # https://smth/uploads/tdpdf/somefile.pdf
        return title, href  # возвращаем пустую ссылку
    soup = BeautifulSoup(response.text, 'html.parser')
    for link in soup.find_all('a'):  # находим все ссылки
        href = link.get('href')
        title = link.text
        # не считаем слеш и якорь ссылкой (продолжаем поиск)
        if href == "/" or href.startswith("#"):
            continue
        # if is_relative(href):  # добавил для наглядности
        #    continue  # пропускаем, ищем только абсолютные
        if not href.startswith("http") \
                and not href.startswith("/") \
                and not href.startswith("?"):
            continue  # пропускаем, не считаем такое за ссылку
        break  # если ссылка подходит, забираем
    return title, href
