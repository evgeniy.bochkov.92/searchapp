# Создать программу поисковик (консольную)
# Пользователь вводит текст запроса, поисковую систему
# (google.com, yandex.ru, ...), количество результатов,
# рекурсивный поиск или нет, формат вывода (в консоль, в файл json, в csv)
# Программа находит в интернете начиная от стартовой точки все ссылки
# на веб-странице в заданном количестве (название ссылки и саму ссылку)
# Если поиск не рекурсивный, то берем ссылки только из поисковика,
# если рекурсивный, то берем первую ссылку,
# переходим, находим там ссылки, переходим, ...
# В зависимости от выбранного формата вывода сохраняем результат
# (текст ссылки: ссылка) либо в консоль либо в файл выбранного формата

from search import output
from search import interaction
from search import page_parser
from search import search_engines


def main():
    # задать текст запроса
    request = interaction.set_request()
    # узнать нужна ли рекурсия
    is_recursive = interaction.check_recursion()
    # задать поисковую систему
    search_engine_index = interaction.set_search_engine()
    # задать количество результатов
    amount_results = interaction.set_amount_results()
    # задать формат вывода
    output_format_index = interaction.set_output_format()
    print("Выполняется поиск...")
    # получить модуль движка
    m = search_engines.get_engine(search_engine_index)
    # выполнить поиск в поисковой системе
    search_result = m.search(request,
                             1 if is_recursive else amount_results)
    # если требуется рекурсивный поиск
    if is_recursive and len(search_result) > 0:
        search_result += page_parser.search_recursively(
            search_result[0].link,
            amount_results-1)  # -1 т.к. одну ссылку уже получили
    # вывести результаты
    save(search_result, output_format_index)


def save(search_result=[], output_format=1):
    """
    Сохранить результаты в различные форматы,
    либо вывести в консоль
    :param search_result: список объектов типа SearchItem
    :param output_format: идентификатор формата вывода
    :return:
    """
    # если search_result = [], файлы создадутся пустыми
    f = output.get_saver(output_format)
    f(search_result)


if __name__ == "__main__":
    main()
