# Модуль взаимодействия с пользователем

from colorama import Fore, Style
from search.search_engines import SEARCH_ENGINES
from search.output import OUTPUT_FORMATS


def set_request():
    """
    Задать поисковый запрос
    :return: поисковый запрос
    """
    request = input("Введите запрос: ")  # задать текст запроса
    print(Fore.GREEN + "Ваш запрос: ", request)
    print(Style.RESET_ALL)
    return request


def check_recursion():
    """
    Требуется ли рекурсивный поиск
    :return: True или False
    """
    while True:
        s = input("Требуется ли рекурсивный поиск (да/нет)? ")
        if s == "да" or s == "д" or s == "y"\
                or s == "yes" or s == "yaasss":
            is_recursive = True
            break
        elif s == "нет" or s == "н" or s == "n"\
                or s == "no":
            is_recursive = False
            break
    print(Fore.GREEN + "Ваш ответ: ", "да" if is_recursive else "нет")
    print(Style.RESET_ALL)
    return is_recursive


def set_search_engine():
    """
    Выбор поисковой системы
    :return: номер поискового движка
    """
    while True:
        print("Выберите поисковый движок:")
        display_search_engines()  # отобразить список движков
        try:
            search_engine_index = int(input())
            if check_search_engine(search_engine_index):
                break  # выходим из цикла
        except ValueError:
            print(Fore.GREEN + f"Требуется ввести число от 1 "
                               f"до {len(SEARCH_ENGINES)}!")
        finally:
            print(Style.RESET_ALL)
    print(Fore.GREEN + "Выбран поисковый движок: ",
          SEARCH_ENGINES[search_engine_index])
    print(Style.RESET_ALL)
    return search_engine_index


def display_search_engines():
    """
    Отобразить список движков
    :return:
    """
    for s in SEARCH_ENGINES:
        print(f"\t{s}: {SEARCH_ENGINES[s]}")


def check_search_engine(search_engine):
    """
    Проверить корректность ввода идентификатора движка
    :param search_engine: ввод пользователя
    :return:
    """
    is_correct = False
    if search_engine > len(SEARCH_ENGINES) or search_engine <= 0:
        print(Fore.GREEN + f"Требуется ввести число от 1 "
                           f"до {len(SEARCH_ENGINES)}!")
    else:
        is_correct = True
    return is_correct


def set_amount_results():
    """
    Ввод количества результатов поиска
    :return: количество результатов поиска
    """
    max_amount_results = 100  # максимально допустимое количество результатов
    while True:
        try:
            amount_results = int(input("Введите требуемое количество "
                                       "результатов поиска: "))
            if amount_results > max_amount_results \
                    or amount_results <= 0:  # ограничение
                print(Fore.GREEN + f"Требуется ввести число от 1 "
                                   f"до {max_amount_results}!")
            else:
                break
        except ValueError:
            print(Fore.GREEN + f"Требуется ввести число от 1 "
                               f"до {max_amount_results}!")
        finally:
            print(Style.RESET_ALL)
    print(Fore.GREEN + "Выбрано количество результатов: ", amount_results)
    print(Style.RESET_ALL)
    return amount_results


def set_output_format():
    """
    Ввод требуемого формата вывода результатов поиска
    :return: номер формата вывода
    """
    while True:
        print("Введите формат вывода:")
        display_output_formats()  # отобразить форматы вывода
        try:
            output_format_index = int(input())
            if output_format_index > len(OUTPUT_FORMATS) \
                    or output_format_index <= 0:
                print(Fore.GREEN + f"Требуется ввести число от 1 "
                                   f"до {len(OUTPUT_FORMATS)}!")
            else:
                break  # выходим из цикла
        except ValueError:
            print(Fore.GREEN + f"Требуется ввести число от 1 "
                               f"до {len(OUTPUT_FORMATS)}!")
        finally:
            print(Style.RESET_ALL)
    print(Fore.GREEN + "Выбран формат вывода: ",
          OUTPUT_FORMATS[output_format_index])
    print(Style.RESET_ALL)
    return output_format_index


def display_output_formats():
    """
    Отобразить форматы вывода
    :return:
    """
    for o in OUTPUT_FORMATS:
        print(f"\t{o}: {OUTPUT_FORMATS[o]}")
