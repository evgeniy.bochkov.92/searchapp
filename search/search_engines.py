# Модуль поисковых движков

import search.google
import search.yahoo
import search.yandex

SEARCH_ENGINES = {  # поисковые движки
    1: 'Google',
    2: 'Yandex (тестовый режим)',
    3: 'Yahoo (тестовый режим)'
}


def get_engine(index):
    """
    Возвращает модуль поискового движка
    по переданному номеру
    :param index: номер поискового движка
    :return: функция вывода
    """
    search_engines = {
        1: search.google,
        2: search.yandex,
        3: search.yahoo
    }
    return search_engines[index]
