# Модуль вывода:
# 1. -> файл json
# 2. -> файл csv
# 3. -> консоль

import json
import csv

OUTPUT_FORMATS = {  # форматы вывода
    1: 'json в файл',
    2: 'csv в файл',
    3: 'print в консоль'
}

# имя файла для выгрузки результатов в csv
CSV_FILENAME = "search_result.csv"
# имя файла для выгрузки результатов в json
JSON_FILENAME = "search_result.json"


def get_saver(index):
    """
    Возвращает функцию вывода
    по переданному номеру
    :param index: номер функции вывода
    :return: функция вывода
    """
    savers = {
        1: save2json,
        2: save2csv,
        3: print2console
    }
    return savers[index]


def save2json(search_result):
    """
    Экспорт списка объектов SearchItem в json файл
    :param search_result: список объектов типа SearchItem
    :return:
    """
    with open(JSON_FILENAME, 'w', encoding="utf-8") as f:
        json.dump([item.__dict__ for item in search_result], f,
                  ensure_ascii=False)
    print("Результаты успешно экспортированы в файл json")


def save2csv(search_result):
    """
    Экспорт списка объектов SearchItem в csv файл
    :param search_result: список объектов типа SearchItem
    :return:
    """
    with open(CSV_FILENAME, "w", newline="", encoding="utf-8") as file:
        writer = csv.writer(file)
        for i in range(0, len(search_result)):
            writer.writerow([i + 1, search_result[i].title,
                             search_result[i].link])
    print("Результаты успешно экспортированы в файл csv")


def print2console(search_result):
    """
    Вывод списка объектов SearchItem в консоль
    :param search_result: список объектов типа SearchItem
    :return:
    """
    for i in range(0, len(search_result)):
        print(f"{i + 1:02}"
              f"\nНазвание ссылки: {search_result[i].title}"
              f"\nАдрес ссылки: {search_result[i].link}")

# def get_json(search_result):
#   """
#   Преобразование списка объектов SearchItem в json строку
#   :param search_result: список объектов типа SearchItem
#   :return: строка json
#   """
#   json_string = json.dumps([item.__dict__ for item in search_result])
#   return json_string
