class SearchItem:
    """
    Класс, описывающий найденный элемент
    """

    def __init__(self, title, link):
        self.title = title  # заголовок
        self.link = link  # ссылка
