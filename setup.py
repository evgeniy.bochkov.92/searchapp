import os
from setuptools import setup, find_packages

# allow setup.py to be run from any path
os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))

setup(
    name='search',
    version='0.1',
    packages=find_packages(),
    include_package_data=True,
    license='GNU General Public License v3.0',
    description='Custom search in search engines with the ability to save results in csv and json formats',
    url='https://gitlab.com/evgeniy.bochkov.92/searchapp.git',
    author='Evgeniy Bochkov',
    author_email='evgeniy.bochkov.92@gmail.com',
    keywords=['search', 'app', 'json', 'csv', 'google', 'engine'],
    entry_points={
        'console_scripts': [
            'start_search=search.main:main',
        ],
    },
)
